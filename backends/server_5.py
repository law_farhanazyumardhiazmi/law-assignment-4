import json
import pika
import time

from datetime import datetime


def main():
    connection_credentials = pika.PlainCredentials(
        username="0806444524", password="0806444524",
    )
    connection_parameters = pika.ConnectionParameters(
        host="152.118.148.95",
        port=5672,
        virtual_host="/0806444524",
        credentials=connection_credentials,
    )
    connection = pika.BlockingConnection(parameters=connection_parameters)
    channel = connection.channel()
    exchange_name = "1706979234_TOPIC"
    channel.exchange_declare(exchange=exchange_name, exchange_type="topic")

    routing_key = "1706979234.TIME"

    try:
        while True:
            body = {"time": str(datetime.now())}
            print(f"Sending message {body}")
            channel.basic_publish(
                exchange=exchange_name,
                routing_key=routing_key,
                body=json.dumps(body),
                mandatory=True,
            )
            time.sleep(0.3)
    except KeyboardInterrupt:
        connection.close()


if __name__ == "__main__":
    main()
