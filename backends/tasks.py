import json
import os
import pika
import requests
import wget
import zipfile
from celery import Celery
from pathlib import Path
from utils import retrieve_file_paths, generate_secret_url

app = Celery("tasks", broker="amqp://localhost")

SERVER_3_URL = "http://localhost:8001/"


exchange_name = "1706979234_TOPIC"
connection_credentials = pika.PlainCredentials(
    username="0806444524", password="0806444524",
)
connection_parameters = pika.ConnectionParameters(
    host="152.118.148.95",
    port=5672,
    virtual_host="/0806444524",
    credentials=connection_credentials,
    heartbeat=600,
    blocked_connection_timeout=300,
)
connection = pika.BlockingConnection(parameters=connection_parameters)
channel = connection.channel()


def publish_message(payload, topic_code):
    channel.exchange_declare(exchange=exchange_name, exchange_type="topic")
    body = json.dumps(payload)
    channel.basic_publish(
        exchange=exchange_name, routing_key=f"{topic_code}.{payload['code']}", body=body
    )


@app.task
def download_file(file_metadata):
    def send_percentage(current, total, order, url, width=80):
        percentage = int((current / total) * 100)
        file_metadata["progress"] = percentage
        publish_message(file_metadata, "download")

    download_progress = lambda current, total, width: send_percentage(
        current, total, file_metadata["order"], file_metadata["url"]
    )
    Path(f"./download/{file_metadata['code']}").mkdir(parents=True, exist_ok=True)
    filename = wget.download(
        url=file_metadata["url"],
        out=f"./download/{file_metadata['code']}",
        bar=download_progress,
    )
    with open(filename, "rb") as file:
        files = {"file": file}
        del file_metadata["progress"]
        requests.post(url=SERVER_3_URL, data=file_metadata, files=files)

    if os.path.exists(filename):
        os.remove(filename)


@app.task
def compress_files_batch(file_metadata):
    batch_code = file_metadata["code"]
    processed_file_paths = retrieve_file_paths(f"server_3/files/{batch_code}")
    cumulative_percentage = cumulative_processed_size = 0
    compressed_file_name = f"{batch_code}_compressed.zip"
    with zipfile.ZipFile(
        f"compress-results/{compressed_file_name}", "w"
    ) as zipped_file:
        for i, file_path in enumerate(processed_file_paths):
            file_object_size = os.path.getsize(file_path)
            zipped_file.write(file_path)
            cumulative_processed_size += file_object_size
            percentage = int(((i + 1) / len(processed_file_paths)) * 100)
            file_metadata["progress"] = percentage
            publish_message(file_metadata, "compress")

    secure_url = generate_secret_url(f"compress-results/{compressed_file_name}")
    publish_message({"code": batch_code, "url": secure_url}, "url")
