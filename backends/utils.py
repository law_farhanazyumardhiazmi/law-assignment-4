import os
import time
from base64 import urlsafe_b64encode
from hashlib import md5


def retrieve_file_paths(directory_path):
    file_paths = []
    for root, directories, files in os.walk(directory_path):
        for filename in files:
            file_path = os.path.join(root, filename)
            file_paths.append(file_path)
    return file_paths


def generate_secret_url(location):
    SERVER_4_HOST = "localhost:8002"
    secret = "LAW-1706979234-NOT-SO-SECRET"
    expire_minutes = 5 * 60
    expire_time = str(int(time.time() + expire_minutes))
    combined_secret = f"{secret}/{location}{expire_time}".encode("utf-8")
    md5_hash = md5(combined_secret).digest()
    base64_hash = urlsafe_b64encode(md5_hash)
    string_hash = base64_hash.decode("utf-8").rstrip("=")
    return f"http://{SERVER_4_HOST}/{location}?md5={string_hash}&expires={expire_time}"
