import json
import os
import pika

from tasks import download_file as task_download_file


def download_file(channel, method, properties, body):
    print(f"Message received: {body}")
    task_download_file.delay(json.loads(body))


def main():
    exchange_name = "1706979234_DIRECT"
    connection_credentials = pika.PlainCredentials(
        username="0806444524", password="0806444524",
    )
    connection_parameters = pika.ConnectionParameters(
        host="152.118.148.95",
        port=5672,
        virtual_host="/0806444524",
        credentials=connection_credentials,
        heartbeat=0,
    )
    connection = pika.BlockingConnection(parameters=connection_parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange_name, exchange_type="direct")
    result = channel.queue_declare(queue="1706979234_DOWNLOAD_URLS")
    queue_name = result.method.queue
    channel.queue_bind(
        queue=queue_name, exchange=exchange_name, routing_key="1706979234_KEY"
    )
    channel.basic_consume(
        queue=queue_name, on_message_callback=download_file, auto_ack=True
    )

    try:
        print("Starts receiving messages...")
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
        print("Connection closed")
        
    connection.close()


if __name__ == "__main__":
    main()
