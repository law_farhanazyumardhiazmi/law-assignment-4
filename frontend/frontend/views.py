import uuid
from django.forms import formset_factory
from django.shortcuts import render
from django.views import View
from .forms import URLForm
from .services import send_urls


class DownloadForm(View):
    formset = formset_factory(URLForm, extra=10)

    def get(self, request, *args, **kwargs):
        return render(request, "frontend/download_form.html", {"formset": self.formset})

    def post(self, request, *args, **kwargs):
        submitted_formset = self.formset(request.POST)
        if submitted_formset.is_valid():
            urls = [form.cleaned_data["url"] for form in submitted_formset]
            batch_code = str(uuid.uuid4())
            send_urls(urls, batch_code)
            return render(
                request,
                "frontend/status.html",
                {"batch_code": batch_code, "urls": urls},
            )
        return render(request, "frontend/download_form.html", {"formset": self.formset})


download_form = DownloadForm.as_view()
