import json
import pika


def create_connection_channel(exchange_name):
    connection_credentials = pika.PlainCredentials(
        username="0806444524", password="0806444524",
    )
    connection_parameters = pika.ConnectionParameters(
        host="152.118.148.95",
        port=5672,
        virtual_host="/0806444524",
        credentials=connection_credentials,
    )
    connection = pika.BlockingConnection(parameters=connection_parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange_name, exchange_type="direct")
    return connection, channel


def send_urls(urls, batch_code):
    payloads = [
        json.dumps({"code": batch_code, "order": i, "url": urls[i]})
        for i in range(len(urls))
    ]
    send_payload(payloads)


def send_payload(payloads):
    exchange_name = "1706979234_DIRECT"
    routing_key = "1706979234_KEY"
    connection, channel = create_connection_channel(exchange_name)
    for payload in payloads:
        channel.basic_publish(
            exchange=exchange_name,
            routing_key=routing_key,
            body=payload,
            mandatory=True,
        )
    channel.close()
    connection.close()
